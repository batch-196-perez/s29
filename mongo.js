// db.products.insertMany(
// 	[
// 		{
// 			"name":"Iphone X",
// 			"price": 30000,
// 			"isActive": true,
// 		},
// 		{
// 			"name":"Samsung Galaxy S21",
// 			"price": 51000,
// 			"isActive": true,
// 		},
// 		{
// 			"name":"Razer Blackshark V2X",
// 			"price": 2800,
// 			"isActive": false,
// 		},
// 		{
// 			"name":"RAKK Gaming Mouse",
// 			"price": 1800,
// 			"isActive": true,
// 		},
// 		{
// 			"name":"RAKK Mechanical Keyboard",
// 			"price": 4000,
// 			"isActive": true,
// 		}
// 	]
// )


// Query Operators
// Allows us to expand our queries and define conditions 
//instead of looking specific values

//$gt, $lt, $gte, $lte
//$gt - query operator which means greater than
db.products.find({price:{$gt:3000}})

//$lt - query operator which means less than
db.products.find({price:{$lt:3000}})

//$gte - query operator which means greater than or equal to
db.products.find({price:{$gte:30000}})   